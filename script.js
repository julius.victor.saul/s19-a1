

let num = 4;
const getCube  = Math.pow(num, 3);
console.log(`The cube of ${num} is ${getCube}`)


let address = ['258','Washingtong Ave.','NW','California','90011'];
const [houseNo,street,state,city,zipCode] = address;
console.log(`I live at ${houseNo} ${street} ${state}, ${city} ${zipCode}`)


const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
}

function getAnimal ({name, species, weight, measurement}){
	console.log(`${name} is a ${species}. He weighed at ${weight} with a measurement of ${measurement}`);
}

getAnimal(animal);


class Dog{
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Daschund")
console.log(myDog)